from Tkinter import *
import time
import random

def iniciar():
    showball()

def desenhaBola(x1, y1, x2, y2, queCor, queTag):
    mapa.create_oval(x1, y1, x2, y2, \
                     fill=queCor,tags=queTag)
def delete():
    for x in range(100):
        mapa.delete("bola"+str(x))
        mapa.update()
        time.sleep(0.03)
    
def showball():
    n=0
    for i in range(100):
        R = random.randint(100, 255)
        G = random.randint(100, 255)
        B = random.randint(100, 255)
        
        x1 = random.randint(50, 550)
        y1 = random.randint(50, 350)
        
        desenhaBola(x1, y1, x1+30, y1+30, '#%02x%02x%02x' % (R,G,B),'bola'+str(n))
        mapa.update()
        time.sleep(0.03)
        n+=1
    
#INTERFACE PRINCIPAL
root=Tk()
root.title('Showball')
options=Frame(root,bg='black')
options.pack(fill='x',expand=True)
botao1=Button(options,text='GO!',command=iniciar)
botao1.pack(side='left',fill='x',expand=True)
botao2=Button(options,text='LIMPAR',command=delete)
botao2.pack(side='left',fill='x',expand=True)

mapa=Canvas(root,width=600,height=400)
mapa.pack()
#mapa.bind("<ButtonRelease-1>", clique)
mainloop()

#EX 2
def iniciar():
        showball()
        
def desenhaBola(x1, y1, x2, y2, queCor, queTag):
    mapa.create_oval(x1, y1, x2, y2, \
                     fill=queCor,tags=queTag)
def muda_cor():
    R = random.randint(100, 255)
    G = random.randint(100, 255)
    B = random.randint(100, 255)

    return (R,G,B)

def showball():
    hor = 5
    ver = 5
    x1 = 0
    y1 = 0
    cor = muda_cor()
    diametro = 100
    
    while True:
        if(x1 + diametro > 600):
            x1 = 600-diametro
            cor = muda_cor()
            hor *= -1

        if(x1 < 0):
            x1 = 0
            cor = muda_cor()
            hor *= -1
        if(y1 + diametro > 400):
            y1 = 400-diametro
            cor = muda_cor()
            ver *= -1

        if(y1 < 0):
            y1 = 0
            cor = muda_cor()
            ver *= -1
            
        x2 = x1 + diametro
        y2 = y1 + diametro
        
        desenhaBola(x1, y1, x2, y2, '#%02x%02x%02x' % cor,'bola')
        mapa.update()
        
        time.sleep(0.03)
        
        #mapa.delete('bola')
        
        x1 += hor
        y1 += ver
    
#INTERFACE PRINCIPAL
root=Tk()
root.title('Showball')
options=Frame(root,bg='black')
options.pack(fill='x',expand=True)
botao1=Button(options,text='GO!',command=iniciar)
botao1.pack(side='left',fill='x',expand=True)

mapa=Canvas(root,width=600,height=400)
mapa.pack()
mainloop()
