 import random

## Daniel de Sousa da Silva DRE118064962
## EX 1
def div(a):
    if(a%2==0 and a%3==0 and a%5==0):
       return True
    
    return False

## Daniel de Sousa da Silva DRE118064962
## EX 2
def EHPRIMO(n):
    if(n==1):
        return False
    
    for i in range(2, n):
        if(n%i==0):
            return False
    return True

## Daniel de Sousa da Silva DRE118064962
## EX 3
def XPTO(s):
    newS=""
    for l in s:
        if(not l in newS):
            newS+=l
            
    return newS

## Daniel de Sousa da Silva DRE118064962
## EX 4
def PALINDROMO(L):
    lista=[]
    for i in range(len(L)):
        if(L[i]==L[i][::-1]):
            lista.append(L[i])
    return lista

## Daniel de Sousa da Silva DRE118064962
## EX 5
def SORTEIO(nome):
    lista = open(nome+".txt").readlines()
    return random.choice(lista).strip()

## Daniel de Sousa da Silva DRE118064962
## EX 6
def UNIAO(arg0,arg1):
    lista = []
    for i in arg0:
        if(not i in lista):
            lista.append(i)
    for j in arg1:
        if(not j in lista):
            lista.append(j)
    return lista

## Daniel de Sousa da Silva DRE118064962
## EX 7
def INTERSECAO(arg0,arg1):
    lista = []
    for i in arg0:
        if(i in arg1 and not i in lista):
            lista.append(i)
    return lista

## Daniel de Sousa da Silva DRE118064962
## EX 8
def DIFERENCA(arg0,arg1):
    lista = []
    for i in arg0:
        if(not i in arg1 and not i in lista):
            lista.append(i)
    for j in arg1:
        if(not j in arg0 and not j in lista):
            lista.append(j)
    return lista

## Daniel de Sousa da Silva DRE118064962
## EX 9
def DOISARQUIVOS(arg0, arg1):
    f = open("arquivo.txt", "w+")
    lista = f.readlines()
    lido1 = open(arg0+".txt").readlines()
    lido2 = open(arg1+".txt").readlines()
    for i in lido1:
        if(i in lido2 and not i in lista):
            lista.append(i)
    for j in range(len(lista)):
        f.write(lista[j])

## Daniel de Sousa da Silva DRE118064962
## EX 10
def AVALIA(g, r):
    acertos = 0
    
    try:
        for i in range(len(g)):
            if(g[i] == r[i]):
                acertos += 1
        return acertos
    except:
        return -1
